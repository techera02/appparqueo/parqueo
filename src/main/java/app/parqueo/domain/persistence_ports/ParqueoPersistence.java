package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.RequestParqueo;

public interface ParqueoPersistence {

	Integer validarParqueo(String tipo, String placa, String codigoEstacionamiento);
	
	Integer registrarEventoParqueo(RequestParqueo request);
}
