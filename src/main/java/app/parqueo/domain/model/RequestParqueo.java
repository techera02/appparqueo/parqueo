package app.parqueo.domain.model;

public class RequestParqueo {

	private String tipo;
	private String placa;
	private String codigoEstacionamiento;
	private String fechaHora;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getCodigoEstacionamiento() {
		return codigoEstacionamiento;
	}
	public void setCodigoEstacionamiento(String codigoEstacionamiento) {
		this.codigoEstacionamiento = codigoEstacionamiento;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
}
