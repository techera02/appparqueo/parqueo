package app.parqueo.infrastructure.postgresql.persistence;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.RequestParqueo;
import app.parqueo.domain.persistence_ports.ParqueoPersistence;
import app.parqueo.infrastructure.postgresql.repositories.ParqueoRepository;

@Repository("ParqueoPersistence")
public class ParqueoPersistencePostgres implements ParqueoPersistence{

	private final ParqueoRepository parqueoRepository;
	
    @Autowired
    public ParqueoPersistencePostgres(ParqueoRepository parqueoRepository) 
    {
        this.parqueoRepository = parqueoRepository;
    }
    
    @Override
	public Integer validarParqueo(String tipo, String placa, String codigoEstacionamiento)
	{
		return this.parqueoRepository.validarParqueo(tipo, placa, codigoEstacionamiento);
	}
	
	@Override
	public Integer registrarEventoParqueo(RequestParqueo request)
	{
		
		Timestamp fechaHora = Timestamp.valueOf(request.getFechaHora());
		
		return this.parqueoRepository.registrarEventoParqueo(request.getTipo(), request.getPlaca(),
				request.getCodigoEstacionamiento(), fechaHora);
	}
}
