package app.parqueo.infrastructure.postgresql.entities;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Parqueo\"")
public class ParqueoEntity {

	@Id
	private Integer Parq_Id;
	private String Parq_Placa;
	private Integer Vehi_Id;
	private Integer Esta_Id;
	private Timestamp Parq_FechaInicio;
	private Timestamp Parq_FechaFin;
	private String Parq_Duracion;
	private BigDecimal Parq_Tarifa;
	private BigDecimal Parq_Costo;
	
	public Integer getParq_Id() {
		return Parq_Id;
	}
	public void setParq_Id(Integer parq_Id) {
		Parq_Id = parq_Id;
	}
	public String getParq_Placa() {
		return Parq_Placa;
	}
	public void setParq_Placa(String parq_Placa) {
		Parq_Placa = parq_Placa;
	}
	public Integer getVehi_Id() {
		return Vehi_Id;
	}
	public void setVehi_Id(Integer vehi_Id) {
		Vehi_Id = vehi_Id;
	}
	public Integer getEsta_Id() {
		return Esta_Id;
	}
	public void setEsta_Id(Integer esta_Id) {
		Esta_Id = esta_Id;
	}
	public Timestamp getParq_FechaInicio() {
		return Parq_FechaInicio;
	}
	public void setParq_FechaInicio(Timestamp parq_FechaInicio) {
		Parq_FechaInicio = parq_FechaInicio;
	}
	public Timestamp getParq_FechaFin() {
		return Parq_FechaFin;
	}
	public void setParq_FechaFin(Timestamp parq_FechaFin) {
		Parq_FechaFin = parq_FechaFin;
	}
	public String getParq_Duracion() {
		return Parq_Duracion;
	}
	public void setParq_Duracion(String parq_Duracion) {
		Parq_Duracion = parq_Duracion;
	}
	public BigDecimal getParq_Tarifa() {
		return Parq_Tarifa;
	}
	public void setParq_Tarifa(BigDecimal parq_Tarifa) {
		Parq_Tarifa = parq_Tarifa;
	}
	public BigDecimal getParq_Costo() {
		return Parq_Costo;
	}
	public void setParq_Costo(BigDecimal parq_Costo) {
		Parq_Costo = parq_Costo;
	}
}
