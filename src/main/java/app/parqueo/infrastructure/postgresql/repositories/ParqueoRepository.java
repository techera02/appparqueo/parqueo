package app.parqueo.infrastructure.postgresql.repositories;

import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.ParqueoEntity;

public interface ParqueoRepository extends JpaRepository<ParqueoEntity, Integer>{

	@Query(value = "SELECT \"UFN_ValidarParqueo\"(:tipo,:placa,:codigoestacionamiento)", nativeQuery = true)
	Integer validarParqueo(@Param("tipo") String tipo, @Param("placa") String placa,
			@Param("codigoestacionamiento") String codigoestacionamiento);
	
	@Query(value = "CALL \"USP_Parqueo_INS_UPD\"(:tipo,:placa,:codigoestacionamiento,:fechahora)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer registrarEventoParqueo(@Param("tipo") String tipo, @Param("placa") String placa,
			@Param("codigoestacionamiento") String codigoestacionamiento, @Param("fechahora") Timestamp fechahora);
}
