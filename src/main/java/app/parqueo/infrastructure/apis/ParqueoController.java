package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.ParqueoService;
import app.parqueo.application.ParqueoValidator;
import app.parqueo.domain.model.RequestParqueo;
import app.parqueo.domain.model.ResponseRegistrarEventoParqueo;

@RestController
@RequestMapping("/Parqueo")
public class ParqueoController {

	private final ParqueoService parqueoService;

	@Autowired
	public ParqueoController(ParqueoService parqueoService) {
		this.parqueoService = parqueoService;
	}

	@PostMapping("/RegistrarEventoParqueo")
	public ResponseEntity<ResponseRegistrarEventoParqueo> registrarEventoParqueo(@RequestBody RequestParqueo request) {
		ResponseRegistrarEventoParqueo response = new ResponseRegistrarEventoParqueo();
		ParqueoValidator parqueoValidator = new ParqueoValidator();

		try {

			response = parqueoValidator.RegistrarEventoParqueo(request);

			if (response.getEstado() == true) {

				Integer temp = this.parqueoService.validarParqueo(request.getTipo(), request.getPlaca(), request.getCodigoEstacionamiento());

				if (temp == 0) {
					this.parqueoService.registrarEventoParqueo(request);
					response.setResultado(true);

				} else {
					
					if(request.getTipo().equals("E")) {
						response.setCodError(5);
						response.setMensaje("Entrada ya existente");
					}else {
						response.setCodError(6);
						response.setMensaje("Salida ya existente");
					}
					
					response.setEstado(false);
				}

			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}

		return ResponseEntity.ok(response);
	}
}
