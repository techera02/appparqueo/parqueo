package app.parqueo.application;

import app.parqueo.domain.model.RequestParqueo;
import app.parqueo.domain.model.ResponseRegistrarEventoParqueo;

public class ParqueoValidator {
	
	public ResponseRegistrarEventoParqueo RegistrarEventoParqueo(RequestParqueo request)
	{
		
		ResponseRegistrarEventoParqueo response = new ResponseRegistrarEventoParqueo();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);
		
		if(request.getTipo() == null) {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Tipo vacio");
		} else if(request.getPlaca() == null) {
			response.setCodError(2);
			response.setEstado(false);
			response.setMensaje("Placa vacio");
		} else if(request.getCodigoEstacionamiento() == null) {
			response.setCodError(3);
			response.setEstado(false);
			response.setMensaje("CodigoEstacionamiento vacio");
		} else if(request.getFechaHora() == null) {
			response.setCodError(4);
			response.setEstado(false);
			response.setMensaje("FechaHora vacio");
		}

		return response;
	}
}
