package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.RequestParqueo;
import app.parqueo.domain.persistence_ports.ParqueoPersistence;

@Service
public class ParqueoService {

	ParqueoPersistence parqueoPersistence;
	
	public ParqueoService(ParqueoPersistence parqueoPersistence)
	{
		this.parqueoPersistence = parqueoPersistence;
	}
	
	public Integer validarParqueo(String tipo, String placa, String codigoEstacionamiento) 
	{
        return this.parqueoPersistence.validarParqueo(tipo, placa, codigoEstacionamiento);
    }
	
	public Integer registrarEventoParqueo(RequestParqueo request)
	{
        return this.parqueoPersistence.registrarEventoParqueo(request);
    }
}
